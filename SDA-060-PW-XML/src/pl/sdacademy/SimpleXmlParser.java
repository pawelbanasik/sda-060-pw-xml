package pl.sdacademy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SimpleXmlParser {

	private final String path = "resources/";

	// Przyklad 1
	public void readXMLFile(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);

		// tak sie tworzy nowa instancje wymuszaja dlaczego jest jedna instancja
		// bo statycznie tworcy stworzyli
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// tak tworzymy nowa instancje
		DocumentBuilder db = dbf.newDocumentBuilder();

		// parsowanie i normalizacja jest konieczna przy czytaniu z pliku
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();

		// pobiera wszystkie elementy o tagu staff w calym dokumencie
		NodeList nList = doc.getElementsByTagName("staff");

		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
				// tym drukuje id
				System.out.println(elem.getAttribute("id"));

				// tym drukuje firstname i lastname pobieram zawartosc element�w
				// (dodaje dwa stringi)
				String staffName = elem.getElementsByTagName("firstname").item(0).getTextContent() + " "
						+ elem.getElementsByTagName("lastname").item(0).getTextContent();
				System.out.println(staffName);
				System.out.println("---------------");
			}
		}
	}

	// Przyklad 2 - zapisywanie do pliku
	public void writeXMLFile(String filename) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("root");
		Element child = doc.createElement("children");
		child.setAttribute("id", "15");
		child.setTextContent("wartosc");

		Element child2 = doc.createElement("children");
		child2.setAttribute("id", "16");
		child2.setTextContent("wartosc druga");

		root.appendChild(child);
		root.appendChild(child2);
		doc.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	}

	// Zadanie 1 - policzyc srednia pensje pracownikow
	public double getAvgSalary(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();

		double sumaPensji = 0;
		double sredniaPensja = 0;
		NodeList nList = doc.getElementsByTagName("staff");
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) n;
				sumaPensji += Double.parseDouble(e.getElementsByTagName("salary").item(0).getTextContent());

			}

		}
		sredniaPensja = sumaPensji / nList.getLength();
		return sredniaPensja;
	}

	// Zadanie 2 - zwrocic liste pracownikow
	public ArrayList<String> getNames(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();

		ArrayList<String> list = new ArrayList<>();

		NodeList nList = doc.getElementsByTagName("staff");
		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;

				String staffName = elem.getElementsByTagName("firstname").item(0).getTextContent() + " "
						+ elem.getElementsByTagName("lastname").item(0).getTextContent();

				list.add(staffName);
			}
		}

		// normalnie to sie iteruje jakbym chcial w okreslony sposob wydrukowac
		return list;

	}

	// zadanie 3 - podanie maksymalnego i minimalnego id
	public void getMinMaxStaffID(String filename) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(this.path + filename);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		doc.getDocumentElement().normalize();

		ArrayList<String> idList = new ArrayList<>();

		NodeList nList = doc.getElementsByTagName("staff");

		for (int i = 0; i < nList.getLength(); i++) {
			Node n = nList.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;

				idList.add(elem.getAttribute("id"));

			}
				
		}
		int maxValue = 0;
		int minValue = 0;
		
		for (int j = 0; j < idList.size(); j++) {
			if (maxValue < Integer.parseInt(idList.get(j))) {
				maxValue = Integer.parseInt(idList.get(j));
			}
		}
		
		for (int k = 0; k < idList.size(); k++) {
			if (Integer.parseInt(idList.get(0)) > Integer.parseInt(idList.get(k))) {
				minValue = Integer.parseInt(idList.get(k));
			} else {
				minValue = Integer.parseInt(idList.get(0));
			}
		}
		
		System.out.println("Max value of id: " + maxValue);
		System.out.println("Min value of id: " + minValue);
		System.out.println("Lista id: " + idList);
	}

	// zadanie 4 - wypisujemy liste studentow do pliku w okreslony sposob
	public void writeStudents(String filename, List<Student> list) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("root");
		Element students = doc.createElement("students");
		
		for(Student currentStudent : list) {
		Element student = doc.createElement("student");
		Element name = doc.createElement("name");
		Element lastname = doc.createElement("lastname");
		Element year = doc.createElement("year");
		name.setTextContent(currentStudent.getName());
		lastname.setTextContent(currentStudent.getLastName());
		year.setTextContent(currentStudent.getYear());

		student.appendChild(name);
		student.appendChild(lastname);
		student.appendChild(year);
		students.appendChild(student);
		}
		root.appendChild(students);
		doc.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	}

	// zadanie 5 - wypisujemy liste firm do pliku w okreslony sposob
	public void writeCompany(String filename, List<Company> list)
			throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		Element root = doc.createElement("root");

		for(Company currentCompany: list) {
			
			Element company = doc.createElement("company");
			Element starts = doc.createElement("starts");
			Element employees = doc.createElement("employees");
			Element vat = doc.createElement("vat");
			
	
			company.setAttribute("name", currentCompany.getValue());
			starts.setTextContent(currentCompany.getStarts());
			employees.setTextContent(currentCompany.getEmployees() + "");
			vat.setTextContent(currentCompany.getVat() + "");
			
			company.appendChild(starts);
			company.appendChild(employees);
			company.appendChild(vat);
			root.appendChild(company);
		}
		
		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);

	}

	// Zadanie 6 - lista ludzi do pliku
	public void savePeople(List<Person> list, String filename)
			throws ParserConfigurationException, TransformerException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element root = doc.createElement("root");
		Element people = doc.createElement("people");

		for (Person person : list) {

			Element currentPerson = doc.createElement("person");
			Element name = doc.createElement("name");
			Element lastName = doc.createElement("lastname");
			Element company = doc.createElement("company");
			Element salary = doc.createElement("salary");
			Element department = doc.createElement("department");
			Element yearOfBorn = doc.createElement("yearOfBorn");

			name.setTextContent(person.getName());
			lastName.setTextContent(person.getLastName());
			company.setTextContent(person.getCompany());
			salary.setTextContent(person.getSalary() + "");
			department.setTextContent(person.getDepartment());
			yearOfBorn.setTextContent(person.getYearOfBorn() + "");

			currentPerson.appendChild(name);
			currentPerson.appendChild(lastName);
			currentPerson.appendChild(company);
			currentPerson.appendChild(salary);
			currentPerson.appendChild(department);
			currentPerson.appendChild(yearOfBorn);

			people.appendChild(currentPerson);
			root.appendChild(people);
		}

		doc.appendChild(root);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();

		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File(this.path + filename));
		t.transform(source, sr);
	}

}
