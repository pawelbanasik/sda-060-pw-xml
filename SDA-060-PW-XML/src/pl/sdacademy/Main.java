package pl.sdacademy;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {

		SimpleXmlParser sxp = new SimpleXmlParser();
		// Przyklad 1 - czytanie z pliku
//		sxp.readXMLFile("staff.xml");
		
		// Przyklad 2 - zapisywanie do pliku
//		sxp.writeXMLFile("staff2.xml");
		
		// Zadanie 1
//		System.out.println(sxp.getAvgSalary("staff.xml"));
		
		// Zadanie 2
		// mozna jednolinijkowo drukowac ArrayList przy pomocy metody toString()
//		System.out.println(sxp.getNames("staff.xml"));
		
		// Zadanie 3 
//		sxp.getMinMaxStaffID("staff.xml");
		
		// Zadanie 4
		
//		List<Student> list = new LinkedList<>();
//		Student student1 = new Student("John", "Simple", "3");
//		Student student2 = new Student("Jane", "Doe", "1");
//		list.add(student1);
//		list.add(student2);
//		
//		sxp.writeStudents("students.xml", list);
		
		// Zadanie 5
		
//		List<Company> list = new LinkedList<>();
//		Company company1 = new Company("name",  "testowa", "2008", 345, 23);
//		Company company2 = new Company("name", "testowa2", "1979", 34345, 40);
//		Company company3 = new Company("name", "testowa3", "1999", 5, 8);
//		
//		list.add(company1);
//		list.add(company2);
//		list.add(company3);
//		
//		sxp.writeCompany("companies.xml", list);
		
		// Zadanie 6
//		List <Person> list = new ArrayList<>();
//		Person person1 = new Person("Pawel", "Banasik", "gddkia", 10, "testowy", 1983);
//		Person person2 = new Person("Michal", "Banasik", "jumar", 11, "testowy", 1987);
//		Person person3 = new Person("Grzegorz", "Banasik", "strabag", 12, "testowy", 1984);
//		Person person4 = new Person("Paulina", "Banasik", "apteka", 13, "testowy", 1985);
//		Person person5 = new Person("Ninka", "Banasik", "zabawa", 14, "testowy", 2014);
//		
//		list.add(person1);
//		list.add(person2);
//		list.add(person3);
//		list.add(person4);
//		list.add(person5);
//		
//		sxp.savePeople(list, "people.xml");
		
	}

}
