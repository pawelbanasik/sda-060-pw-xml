package pl.sdacademy;

public class Company {
	
	private String name;
	private String value;
	private String starts;
	private int employees;
	private int vat;
	
	
	
	public Company(String name, String value, String starts, int employees, int vat) {
		super();
		this.name = name;
		this.value = value;
		this.starts = starts;
		this.employees = employees;
		this.vat = vat;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getStarts() {
		return starts;
	}
	public void setStarts(String starts) {
		this.starts = starts;
	}
	public int getEmployees() {
		return employees;
	}
	public void setEmployees(int employees) {
		this.employees = employees;
	}
	public int getVat() {
		return vat;
	}
	public void setVat(int vat) {
		this.vat = vat;
	}
	

	
	
}
