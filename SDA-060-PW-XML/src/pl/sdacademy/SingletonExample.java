package pl.sdacademy;

public class SingletonExample {

	// pokazuje jak tworzyc instancje i uzywac
	// odrebny przyklad
	private static SingletonExample instance;

	private SingletonExample() {

	}

	private static SingletonExample getInstance() {
		// lazy initialization
		if (instance == null) {
			System.out.println("tworze instancje!");
			instance = new SingletonExample();

		}
		System.out.println("Zwracam instancje");
		return instance;
	}

	public void getText(String txt) {
		System.out.println(txt);
	}
}
