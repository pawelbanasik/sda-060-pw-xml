package pl.sdacademy;

public class Student {

	private String name;
	private String lastName;
	private String year;
	
	public Student(String name, String lastName, String year) {
		this.name = name;
		this.lastName = lastName;
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	
	

	
	
}
